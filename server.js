const PORT = 3000

const WebSocket = require("ws")
const server = new WebSocket.Server({ port: PORT })

server.on("connection", ws => {
  console.log("User connected")

  ws.on("message", message => {
    server.clients.forEach(client => {
      if (client.readyState === WebSocket.OPEN) {
        client.send(message)
      }
    })
  })
})

console.log(`Server is listening on port ${PORT}`)
