const socket = new WebSocket("ws://localhost:3000")
socket.binaryType = "blob"

const color = getRandomColor()
document.getElementById("sendButton").style.backgroundColor = getRandomColor()

socket.addEventListener("message", event => {
  if (event.data instanceof Blob) {
    const reader = new FileReader()

    reader.onload = function () {
      const text = reader.result
      appendMessageToChat(text)
    }

    reader.readAsText(event.data)
  } else {
    appendMessageToChat(event.data)
  }
})

document
  .getElementById("messageBox")
  .addEventListener("keydown", function (event) {
    if (event.key === "Enter" && event.metaKey) {
      event.preventDefault()
      sendMessage()
    }
  })

function appendMessageToChat(message) {
  const li = document.createElement("li")
  li.textContent = message
  document.getElementById("chatMessages").appendChild(li)
}

function sendMessage() {
  const message = `${color}: ${document.getElementById("messageBox").value}`
  socket.send(message)
  document.getElementById("messageBox").value = ""
}

function getRandomColor() {
  const getRandomValue = () =>
    Math.floor(Math.random() * 256)
      .toString(16)
      .padStart(2, "0")
  return `#${getRandomValue()}${getRandomValue()}${getRandomValue()}`
}
